package lab3;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class PiMonteCarloSkeletonTest {
    private PiMonteCarloSkeleton piMonteCarloSkeleton;

    @BeforeEach
    void setUp() {
        this.piMonteCarloSkeleton = new PiMonteCarloSkeleton();
        this.piMonteCarloSkeleton.setAlive();
    }

    @Test
    @DisplayName("Seed: 31415, S:1000")
    void testCaseOne() {
        this.piMonteCarloSkeleton.setSeed(31415);
        this.piMonteCarloSkeleton.setThreshold(1000);
        this.piMonteCarloSkeleton.run();
        assertEquals(0.788, this.piMonteCarloSkeleton.getRatio(),0.0001);
        assertEquals(3.1520000000, this.piMonteCarloSkeleton.getPI(),0.0001);
    }

    @Test
    @DisplayName("Seed: 13579, S:1000")
    void testCaseTwo() {
        this.piMonteCarloSkeleton.setSeed(13579);
        this.piMonteCarloSkeleton.setThreshold(1000);
        this.piMonteCarloSkeleton.run();
        assertEquals(0.797, this.piMonteCarloSkeleton.getRatio(),0.0001);
        assertEquals(3.1880000000, this.piMonteCarloSkeleton.getPI(),0.0001);
    }

    @Test
    @DisplayName("Seed: 2357, S:1000")
    void testCaseThree() {
        this.piMonteCarloSkeleton.setSeed(2357);
        this.piMonteCarloSkeleton.setThreshold(1000);
        this.piMonteCarloSkeleton.run();
        assertEquals(0.792, this.piMonteCarloSkeleton.getRatio(),0.0001);
        assertEquals(3.1680000000, this.piMonteCarloSkeleton.getPI(),0.0001);
    }

    @Test
    @DisplayName("Seed: 2468, S:1000")
    void testCaseFour() {
        this.piMonteCarloSkeleton.setSeed(2468);
        this.piMonteCarloSkeleton.setThreshold(1000);
        this.piMonteCarloSkeleton.run();
        assertEquals(0.788, this.piMonteCarloSkeleton.getRatio(),0.0001);
        assertEquals(3.1520000000, this.piMonteCarloSkeleton.getPI(),0.0001);
    }
}