package lab4;

public class LinkedList<T> implements Queue<T> {

    private Node<T> head;
    private Node<T> tail;

    /**
     * add one element to the end of the queue
     */
    @Override
    public boolean offer(T t) {
        if (t == null) throw new NullPointerException();

        final Node<T> l = this.tail;
        final Node<T> newNode = new Node<>(l, t, null);

        this.tail = newNode;

        if (l == null) {
            this.head = newNode;
        } else {
            l.next = newNode;
        }

        return true;
    }

    /**
     * remove the first element in the queue and return it
     */
    @Override
    public T poll() {
        if (this.head == null) {
            throw new IllegalStateException();
        }

        final Node<T> f = this.head;
        final T element = f.item;
        final Node<T> next = f.next;

        f.item = null;
        f.next = null;

        this.head = next;

        if (next == null) {
            tail = null;
        } else {
            next.prev = null;
        }
        return element;
    }

    /**
     * return the first element in the queue but not remove
     */
    @Override
    public T peek() {
        if (this.head == null) {
            return null;
        }

        final Node<T> f = this.head;
        return f.item;
    }

    /**
     * inner class
     */

    private static class Node<E> {
        E item;
        Node<E> next;
        Node<E> prev;

        Node(Node<E> prev, E element, Node<E> next) {
            this.item = element;
            this.next = next;
            this.prev = prev;
        }
    }
}
