package lab4;

import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import java.io.File;
import java.util.stream.IntStream;

/**
 * skeleton of fractal application
 *
 * @author Shahriar (Shawn) Emami
 * @version May 24, 2018
 */
public class PiMonteCarloApp extends Application {

    private GraphicsContext gc;
    private Canvas canvas;
    private PiMonteCarloSkeleton piMonteCarlo;
    private Animator animator;
    private Label status;
    private Spinner<Long> threshold;

    /**
     * map a number in range of A to B to a new range of C to D
     *
     * @param num    - value to be converted in new range
     * @param orgLow - low bound of original range
     * @param orgHi  - high bound of original range
     * @param newLow - low bound of new range
     * @param newHi  - high bound of new range
     * @return
     */
    public static double map(double num, double orgLow, double orgHi, double newLow, double newHi) {
        return (num - orgLow) * (newHi - newLow) / (orgHi - orgLow) + newLow;
    }

    public static void main(String[] args) {
        launch(args);
    }

    /**
     * init none JavaFX options
     */
    public void init() throws Exception {
        super.init();
        animator = new Animator();
        canvas = new Canvas();
        piMonteCarlo = new PiMonteCarloSkeleton();
        status = new Label();
        this.threshold = new Spinner<>();
    }

    /**
     * create and connect components of JavaFX application
     *
     * @param primaryStage - the primary stage which to the scene can be set.
     */
    public void start(Stage primaryStage) throws Exception {

        BorderPane rootPane = new BorderPane();

        Pane controlPane = createController("Controls", 1000000000, 10000000);

        canvas = new Canvas(800, 800);

        gc = canvas.getGraphicsContext2D();

        BorderPane canvasPane = new BorderPane();
        canvasPane.setStyle("-fx-background-color: LightGray;");
        canvasPane.setCenter(canvas);

        rootPane.setTop(controlPane);

        rootPane.setCenter(canvasPane);

        Scene scene = new Scene(rootPane);

        primaryStage.setScene(scene);
        primaryStage.setTitle("Monte Carlo");

        if (new File("/css/MonteCarlo.css").exists()) {
            scene.getStylesheets().add(PiMonteCarloApp.class.getResource("/css/MonteCarlo.css").toExternalForm());
        }
        if (new File("resources/css/style.css").exists()) {
            scene.getStylesheets().add(new File("resources/css/style.css").toURI().toString());
        }

        primaryStage.show();

        rootPane.widthProperty().addListener((observable, oldValue, newValue) -> {
            canvas.setWidth((Double) newValue);
            clearCanvas();
        });
        rootPane.heightProperty().addListener((observable, oldValue, newValue) -> {
            canvas.setHeight((Double) newValue - controlPane.getHeight());
            clearCanvas();
        });
        clearCanvas();
    }

    /**
     * called after the JavaFX GUI is closed
     */
    public void stop() throws Exception {
        super.stop();
        animator.stop();
        piMonteCarlo.stop();
    }

    /**
     * create the controller pane
     *
     * @param name         - name of the row
     * @param maxSpinner   - max value of spinner
     * @param spinnerValue - starting value of spinner
     * @return a Pane with all components added to it
     */
    private Pane createController(String name, double maxSpinner, double spinnerValue) {

        GridPane gridPane = new GridPane();
        gridPane.setHgap(20);
        gridPane.setVgap(20);
        gridPane.setPadding(new Insets(10, 10, 10, 10));

        Label nameOfRow = new Label(name);
        gridPane.add(nameOfRow, 0, 0);

        Button startButton = new Button("start");
        Button stopButton = new Button("stop");
        Button clearButton = new Button("clear");
        gridPane.add(startButton, 1, 0);
        gridPane.add(stopButton, 2, 0);
        gridPane.add(clearButton, 3, 0);

        Label nameOfSpinner = new Label("Threshold");
        gridPane.add(nameOfSpinner, 4, 0);

        threshold = new Spinner<>(0, maxSpinner, spinnerValue);
        gridPane.add(threshold, 5, 0);

        Label statusLabel = new Label();

        gridPane.add(statusLabel, 6, 0);
        gridPane.add(status, 7, 0);

        threshold.setValueFactory(new SpinnerValueFactory<Long>() {
            {
                setValue(100000L);
            }

            @Override
            public void increment(int steps) {
                setValue(getValue() + 10000);
            }

            @Override
            public void decrement(int steps) {
                setValue(getValue() - 10000);
            }
        });

        threshold.valueFactoryProperty().addListener(event -> clearCanvasAndStart());

        clearButton.setOnAction(event -> clearCanvas());
        startButton.setOnAction(event -> clearCanvasAndStart());
        stopButton.setOnAction(event -> stopAnimatorAndPi());

        return gridPane;
    }

    /**
     * clear the canvas and stop any active animation
     */
    private void clearCanvas() {

        stopAnimatorAndPi();

        if (gc != null) {
            gc.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
            gc.strokeOval(0, 0, canvas.getWidth(), canvas.getHeight());
        }
    }

    /**
     * clear the canvas first and start the animation
     */
    private void clearCanvasAndStart() {

        clearCanvas();

        if (threshold != null) {
            status.setText(threshold.getPromptText());
        }

        if (piMonteCarlo != null) {
            animator.start();
            piMonteCarlo.simulate(threshold.getValue(), animator::addPoint);
        }

    }

    /**
     * stop the animator and PiMonteCarlo thread
     */
    private void stopAnimatorAndPi() {
        animator.stop();
        if (piMonteCarlo != null) piMonteCarlo.stop();
    }

    /**
     * Animation class to animate based on the content of handle method
     *
     * @author Shahriar (Shawn) Emami
     * @version May 24, 2018
     */
    private class Animator extends AnimationTimer {
        Queue<double[]> pointQueue = new LinkedList<>();

        public void addPoint(double[] point) {
            this.pointQueue.offer(point);
        }

        /**
         * this method is periodically called by AnimationTimer class, there is no need to call it.
         *
         * @param now - current time which handle is called
         */
        @Override
        public void handle(long now) {
            if (pointQueue.peek() == null) return;
            IntStream.range(0, 1000).forEach(value -> {
                double[] doubles = this.pointQueue.poll();
                if (doubles[2] == 0) {
                    gc.setFill(Color.DEEPPINK);
                    gc.fillOval(map(doubles[0], 0, 1, 0, canvas.getWidth()), map(doubles[1], 0, 1, 0, canvas.getHeight()), 1, 1);
                } else {
                    gc.setFill(Color.LIGHTPINK);
                    gc.fillOval(map(doubles[0], 0, 1, 0, canvas.getWidth()), map(doubles[1], 0, 1, 0, canvas.getHeight()), 1, 1);

                }
            });
            status.setText(String.valueOf(piMonteCarlo.getPI()));
        }
    }
}
