package lab2;

import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Spinner;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import lab1.Queue;

import java.io.File;

/**
 * skeleton of fractal application
 *
 * @author Shahriar (Shawn) Emami
 * @version May 24, 2018
 */
public class PiMonteCarlo extends Application {

    private GraphicsContext graphicsContext;
    private Canvas canvas;
    private PiMonteCarlo piMonteCarlo;
    private Animator animator;

    /**
     * map a number in range of A to B to a new range of C to D
     *
     * @param num    - value to be converted in new range
     * @param orgLow - low bound of original range
     * @param orgHi  - high bound of original range
     * @param newLow - low bound of new range
     * @param newHi  - high bound of new range
     * @return
     */
    public static double map(double num, double orgLow, double orgHi, double newLow, double newHi) {
        return (num - orgLow) * (newHi - newLow) / (orgHi - orgLow) + newLow;
    }

    public static void main(String[] args) {
        launch(args);
    }

    /**
     * init none JavaFX options
     */
    public void init() throws Exception {
        super.init();
        animator = new Animator();
        piMonteCarlo = new PiMonteCarlo();
    }

    /**
     * create and connect components of JavaFX application
     *
     * @param primaryStage - the primary stage which to the scene can be set.
     */
    public void start(Stage primaryStage) throws Exception {

        BorderPane rootPane = new BorderPane();

        Pane controlPane = createController("Controls", 0, 1000000000);

        canvas = new Canvas(800, 800);

        graphicsContext = canvas.getGraphicsContext2D();

        BorderPane canvasPane = new BorderPane();
        canvasPane.setStyle("-fx-background-color: LightGray;");
        canvasPane.setCenter(canvas);

        rootPane.setTop(controlPane);

        rootPane.setCenter(canvasPane);

        Scene scene = new Scene(rootPane);

        primaryStage.setScene(scene);
        primaryStage.setTitle("Monte Carlo");

        if (new File("/css/MonteCarlo.css").exists()) {
            scene.getStylesheets().add(PiMonteCarlo.class.getResource("/css/MonteCarlo.css").toExternalForm());
        }
        if (new File("resources/css/style.css").exists()) {
            scene.getStylesheets().add(new File("resources/css/style.css").toURI().toString());
        }

        primaryStage.show();

        rootPane.widthProperty().addListener((observable, oldValue, newValue) -> {
            canvas.setWidth((Double) newValue);
        });
        rootPane.heightProperty().addListener((observable, oldValue, newValue) -> {
            canvas.setHeight((Double) newValue - controlPane.getHeight());
        });

        clearCanvas();
    }

    /**
     * called after the JavaFX GUI is closed
     */
    public void stop() throws Exception {
//        super.stop();
//        animator.stop();
//        piMonteCarlo.stop();
    }

    /**
     * create the controller pane
     *
     * @param gridPane     - layout to hold all the components
     * @param name         - name of the row
     * @param maxSpinner   - max value of spinner
     * @param spinnerValue - starting value of spinner
     * @return a Pane with all components added to it
     */
    private Pane createController(String name, double maxSpinner, double spinnerValue) {

        GridPane gridPane = new GridPane();
        gridPane.setHgap(20);
        gridPane.setVgap(20);
        gridPane.setPadding(new Insets(10, 10, 10, 10));

        Label nameOfRow = new Label(name);
        gridPane.add(nameOfRow, 0, 0);

        Button startButton = new Button("start");
        Button stopButton = new Button("stop");
        Button clearButton = new Button("clear");
        gridPane.add(startButton, 1, 0);
        gridPane.add(stopButton, 2, 0);
        gridPane.add(clearButton, 3, 0);

        Label nameOfSpinner = new Label("Threshold");
        gridPane.add(nameOfSpinner, 4, 0);

        Spinner<Integer> spinner = new Spinner<>(0, maxSpinner, spinnerValue);
        gridPane.add(spinner, 5, 0);

        Label statusLabel = new Label("");

        gridPane.add(statusLabel, 6, 0);

        spinner.valueFactoryProperty().addListener(event -> clearCanvasAndStart(statusLabel));

        clearButton.setOnAction(event -> clearCanvas());
        startButton.setOnAction(event -> clearCanvasAndStart(statusLabel));
        stopButton.setOnAction(event -> stopAnimatorAndPi());

        return gridPane;
    }

    /**
     * clear the canvas and stop any active animation
     */
    private void clearCanvas() {

        graphicsContext.clearRect(0, 0, canvas.getHeight(), canvas.getWidth());

        graphicsContext.strokeOval(0, 0, canvas.getHeight(), canvas.getWidth());
    }

    /**
     * clear the canvas first and start the animation
     *
     * @param label - output location to print animation details
     */
    private void clearCanvasAndStart(Label label) {
        clearCanvas();

        if (label != null) {
            label.setText("label is not null");
        }

        if (piMonteCarlo != null) {
            animator.start();
        }
        //TODO start the PiMonteCarlo thread by calling simulate
    }

    /**
     * stop the animator and PiMonteCarlo thread
     */
    private void stopAnimatorAndPi() {
    }

    /**
     * Animation class to animate based on the content of handle method
     *
     * @author Shahriar (Shawn) Emami
     * @version May 24, 2018
     */
    private class Animator extends AnimationTimer {
        Queue pointQueue;

        public void addPoint() {

        }

        /**
         * this method is periodically called by AnimationTimer class, there is no need to call it.
         *
         * @param now - current time which handle is called
         */
        @Override
        public void handle(long now) {
        }

    }
}
