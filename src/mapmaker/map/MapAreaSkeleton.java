package mapmaker.map;

import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Shape;
import mapmaker.shapes.LineSkeleton;
import mapmaker.shapes.PolyShapeSkeleton2;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class MapAreaSkeleton extends Pane {

    /**
     * instead of calling getChildren every time you can call directly the reference of it which is initialized in constructor
     */
    private ObservableList<Node> children;
    /**
     * active shape that is currently being manipulated
     */
    private Shape activeShape;
    /**
     * /**
     * last location of the mouse
     */
    private double startX, startY;
    /**
     * Reference to ToolSate so you don't have to call ToolSate.getState() every time.
     */
    private ToolStateSkeleton tool;

    public MapAreaSkeleton() {
        super();
        tool = ToolStateSkeleton.state();
        children = this.getChildren();
        registerMouseEvents();
    }

    private void registerMouseEvents() {
        addEventHandler(MouseEvent.MOUSE_PRESSED, this::pressClick);
        addEventHandler(MouseEvent.MOUSE_RELEASED, this::releaseClick);
        addEventHandler(MouseEvent.MOUSE_DRAGGED, this::dragClick);
    }

    private void pressClick(MouseEvent mouseEvent) {
        mouseEvent.consume();
        startX = mouseEvent.getX();
        startY = mouseEvent.getY();
        switch (activeTool()) {
            case Door:
            case Move:
                if (mouseEvent.getTarget() instanceof Shape) activeShape = (Shape) mouseEvent.getTarget();
                children.removeAll(((PolyShapeSkeleton2) activeShape).getControlPoints());
                break;
            case Path:
            case Select:
            case Erase:
                if (mouseEvent.getTarget() instanceof Shape) activeShape = (Shape) mouseEvent.getTarget();
                break;
            case Room:
                if (ToolStateSkeleton.state().getOptions() == 0) {
                    activeShape = new LineSkeleton();
                    children.add(activeShape);
                } else {
                    activeShape = new PolyShapeSkeleton2(tool.getOptions());
                    children.add(activeShape);
                }

                break;
            default:
                throw new UnsupportedOperationException("Cursor for Tool " + activeTool().name() + " is not implemneted");
        }
    }

    private void dragClick(MouseEvent mouseEvent) {
        mouseEvent.consume();
        switch (activeTool()) {
            case Door:
            case Path:
            case Erase:
                break;
            case Select:
                break;
            case Move:
                activeShape.relocate(mouseEvent.getX(), mouseEvent.getY());
                break;
            case Room:
                //if you are not using PolyShapeSkeleton2 use line below
                //activeShape.reDraw( startX, startY, distance(startX, startY, e.getX(), e.getY()));
                if (ToolStateSkeleton.state().getOptions() == 0) {
                    ((LineSkeleton) activeShape).setStartX(startX);
                    ((LineSkeleton) activeShape).setStartY(startY);
                    ((LineSkeleton) activeShape).setEndX(mouseEvent.getX());
                    ((LineSkeleton) activeShape).setEndY(mouseEvent.getY());
                } else {
                    ((PolyShapeSkeleton2) activeShape).reDraw(startX, startY, mouseEvent.getX(), mouseEvent.getY(), true);
                }
                break;
            default:
                throw new UnsupportedOperationException("Drag for Tool \"" + activeTool().name() + "\" is not implemneted");
        }
    }

    private void releaseClick(MouseEvent mouseEvent) {
        mouseEvent.consume();
        switch (activeTool()) {
            case Door:
            case Move:
                break;
            case Path:
            case Select:
                break;
            case Erase:
                if (mouseEvent.getTarget() instanceof PolyShapeSkeleton2) {
                    children.remove(activeShape);
                    children.removeAll(((PolyShapeSkeleton2) activeShape).getControlPoints());
                } else {
                    children.remove(activeShape);
                    children.removeAll(((LineSkeleton) activeShape).getControlPoints());
                }
                break;
            case Room:
                if (ToolStateSkeleton.state().getOptions() == 0) {
                    ((LineSkeleton) activeShape).registerControlPoints();
                    children.addAll(((LineSkeleton) activeShape).getControlPoints());
                } else {
                    ((PolyShapeSkeleton2) activeShape).registerControlPoints();
                    children.addAll(((PolyShapeSkeleton2) activeShape).getControlPoints());
                }

                break;
            default:
                throw new UnsupportedOperationException("Release for Tool \"" + activeTool().name() + "\" is not implemneted");
        }
        activeShape = null;
    }

    private Tools activeTool() {
        return tool.getTool();
    }

    public String convertToString() {
        return children.stream().filter(PolyShapeSkeleton2.class::isInstance).map(PolyShapeSkeleton2.class::cast).map(PolyShapeSkeleton2::convertToString).collect(Collectors.joining(System.lineSeparator()));
    }

    public void convertFromString(Map<Object, List<String>> map) {
        map.keySet().stream().map(k -> new PolyShapeSkeleton2(map.get(k))).forEach(s -> {
            children.add(s);
            children.addAll(s.getControlPoints());
        });
    }

    public void clearMap() {
        children.clear();
    }
}
