package mapmaker.map;


public final class ToolStateSkeleton {

    private static final ToolStateSkeleton STATE = new ToolStateSkeleton();

    private Tools tool = Tools.Room;
    private int options = 4;

    private ToolStateSkeleton() {
    }

    public static ToolStateSkeleton state() {
        return STATE;
    }

    public Tools getTool() {
        return tool;
    }

    public void setTool(Tools tool) {
        this.tool = tool;
        options = 0;
    }

    int getOptions() {
        return options;
    }

    public void setOptions(int options) {
        this.options = options;
    }
}
