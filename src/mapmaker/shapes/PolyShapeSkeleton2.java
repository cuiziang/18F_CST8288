package mapmaker.shapes;

import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Polygon;
import mapmaker.shapes.controls.ControlPointSkeleton;

import java.util.List;
import java.util.function.DoubleUnaryOperator;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * http://dimitroff.bg/generating-vertices-of-regular-n-sided-polygonspolyhedra-and-circlesspheres/
 *
 * @author Shahriar (Shawn) Emami
 * @version Sep 27, 2018
 */
public class PolyShapeSkeleton2 extends Polygon {

    private static final String POINTS_COUNT = "sides";
    private static final String FILL = "fill";
    private static final String STROKE = "stroke";
    private static final String WIDTH = "strokeWidth";
    private static final String POINTS = "points";

    private final ObservableList<Double> POLY_POINTS;

    private int sides;
    private double angle;
    private double dx, dy;
    private double x1, y1;

    private ControlPointSkeleton[] cPoints;

    public PolyShapeSkeleton2(int sides) {
        super();
        POLY_POINTS = getPoints();
        this.sides = sides;
    }

    public PolyShapeSkeleton2(List<String> list) {
        this();
        convertFromString(list);
        registerControlPoints();
    }

    private PolyShapeSkeleton2() {
        super();
        POLY_POINTS = getPoints();
    }

    private void cacluatePoints() {
        for (int side = 0; side < sides; side++) {
            POLY_POINTS.addAll(point(Math::cos, dx / 2, angle, side, sides) + x1, point(Math::sin, dy / 2, angle, side, sides) + y1);
        }
    }

    private double radianShift(double x1, double y1, double x2, double y2) {
        return Math.atan2(y2 - y1, x2 - x1);
    }

    private double point(DoubleUnaryOperator operation, double radius, double shift, double side, final int SIDES) {
        return radius * operation.applyAsDouble(shift + side * 2.0 * Math.PI / SIDES);
    }

    public void registerControlPoints() {
        cPoints = new ControlPointSkeleton[POLY_POINTS.size() / 2];
        IntStream.iterate(0, i -> i < POLY_POINTS.size(), i -> i + 2).forEach(i -> {
            final int j = i;
            cPoints[i / 2] = new ControlPointSkeleton(POLY_POINTS.get(i), POLY_POINTS.get(i + 1));
            cPoints[i / 2].addChangeListener((value, vOld, vNew) -> POLY_POINTS.set(j, vNew.doubleValue()), (value, vOld, vNew) -> POLY_POINTS.set(j + 1, vNew.doubleValue()));
        });
    }

    /**
     * measure the distance between 2 points
     */
    private double distance(double x1, double y1, double x2, double y2) {
        return Math.sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
    }

    /**
     * redraw the shape without the need to remake it. till all redrawing are done
     * registerControlPoints should not be called.
     */
    public void reDraw(double x1, double y1, double x2, double y2, boolean symmetrical) {
        angle = radianShift(x1, y1, x2, y2);
        dx = symmetrical ? distance(x1, y1, x2, y2) : x2 - x1;
        dy = symmetrical ? dx : y2 - y1;
        this.x1 = x1 + (x2 - x1) / 2;
        this.y1 = y1 + (y2 - y1) / 2;
        POLY_POINTS.clear();
        cacluatePoints();
    }

    public String convertToString() {
        String newLine = System.lineSeparator();
        StringBuilder builder = new StringBuilder();
        builder.append(POINTS_COUNT).append(" ").append(sides).append(newLine);
        builder.append(FILL).append(" ").append(colorToString(getFill())).append(newLine);
        builder.append(STROKE).append(" ").append(colorToString(getStroke())).append(newLine);
        builder.append(WIDTH).append(" ").append(getStrokeWidth()).append(newLine);
        builder.append(POINTS).append(" ").append(POLY_POINTS.stream().map(e -> Double.toString(e)).collect(Collectors.joining(" ")));

        return builder.toString();
    }

    private void convertFromString(List<String> list) {
        list.forEach(line -> {
            String[] tokens = line.split(" ");
            switch (tokens[0]) {
                case POINTS_COUNT:
                    sides = Integer.valueOf(tokens[1]);
                    break;
                case FILL:
                    setFill(stringToColor(tokens[1], tokens[2]));
                    break;
                case STROKE:
                    setStroke(stringToColor(tokens[1], tokens[2]));
                    break;
                case WIDTH:
                    setStrokeWidth(Double.valueOf(tokens[1]));
                    break;
                case POINTS:
                    Stream.of(tokens).skip(1).mapToDouble(Double::valueOf).forEach(POLY_POINTS::add);
                    break;
                default:
                    throw new UnsupportedOperationException("\"" + tokens[0] + "\" is not supported");
            }
        });
    }

    public Node[] getControlPoints() {
        return cPoints;
    }

    private String colorToString(Paint p) {
        return colorToString(Color.class.cast(p));
    }

    private String colorToString(Color c) {
        return String.format("#%02X%02X%02X %f", (int) (c.getRed() * 255), (int) (c.getGreen() * 255), (int) (c.getBlue() * 255), c.getOpacity());
    }

    private Color stringToColor(String color, String alpha) {
        return Color.web(color, Double.valueOf(alpha));
    }
}
