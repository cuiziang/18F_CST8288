package mapmaker.shapes;

import javafx.scene.Node;
import javafx.scene.shape.Line;
import mapmaker.shapes.controls.ControlPointSkeleton;

public class LineSkeleton extends Line {

    private ControlPointSkeleton[] cPoints;

    public LineSkeleton() {
        super();
    }

    public void registerControlPoints() {
        cPoints = new ControlPointSkeleton[2];
        cPoints[0] = new ControlPointSkeleton(getStartX(), getStartY());
        cPoints[1] = new ControlPointSkeleton(getEndX(), getEndY());
    }

    public Node[] getControlPoints() {
        return cPoints;
    }
}
