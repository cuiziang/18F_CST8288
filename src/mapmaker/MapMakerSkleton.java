package mapmaker;


import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;
import mapmaker.map.MapAreaSkeleton;
import mapmaker.map.ToolStateSkeleton;
import mapmaker.map.Tools;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class MapMakerSkleton extends Application {

    //https://stackoverflow.com/a/45981297/764951
    public static final String REGEX_DECIMAL = "-?(([1-9][0-9]*)|0)?(\\.[0-9]*)?";
    public static final String REGEX_POSITIVE_INTEGER = "([1-9][0-9]*)";

    public static final Pattern P = Pattern.compile(REGEX_POSITIVE_INTEGER);

    public static final String MAPS_DIRECTORY = "resources/maps";
    public static final String INFO_PATH = "resources/icons/info.txt";
    public static final String HELP_PATH = "resources/icons/help.txt";
    public static final String CREDITS_PATH = "resources/credits.txt";

    private MapAreaSkeleton map;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void init() throws Exception {
        super.init();
    }

    @Override
    public void start(Stage primaryStage) throws Exception {

        BorderPane root = new BorderPane();

        Label bottomLabel = new Label();
        ToolBar bottomToolBar = new ToolBar(bottomLabel);

        MenuBar menuBar = new MenuBar(new Menu("File", null, createMenuItem("New", event -> {
        }), createMenuItem("Open", event -> {
        }), createMenuItem("Save", event -> {
        }), new SeparatorMenuItem(), createMenuItem("Exit", event -> primaryStage.hide())), new Menu("Help", null, createMenuItem("Credit", event -> displayCredit()), createMenuItem("Info", event -> {
            displayAlert("Info", loadFile(INFO_PATH));
        }), new SeparatorMenuItem(), createMenuItem("Help", event -> {
            displayAlert("Help", loadFile(HELP_PATH));
        })));

        Button selectButton = new Button();
        selectButton.setId("Select");

        Button moveButton = new Button();
        moveButton.setId("Move");
        moveButton.setOnMouseClicked(event -> {
            ToolStateSkeleton.state().setTool(Tools.Move);
            bottomLabel.setText("Tool: Move");
        });

        MenuButton roomMenuButton = new MenuButton();
        MenuItem lineMenuItem = new MenuItem("Line");
        lineMenuItem.setOnAction(event -> {
            ToolStateSkeleton.state().setTool(Tools.Room);
            ToolStateSkeleton.state().setOptions(0);
            bottomLabel.setText("Tool: Room, Option: Line");
        });

        MenuItem triangleMenuItem = new MenuItem("Triangle");
        triangleMenuItem.setOnAction(event -> {
            ToolStateSkeleton.state().setTool(Tools.Room);
            ToolStateSkeleton.state().setOptions(3);
            bottomLabel.setText("Tool: Room, Option: Triangle");
        });

        MenuItem rectangleMenuItem = new MenuItem("Rectangle");
        rectangleMenuItem.setOnAction(event -> {
            ToolStateSkeleton.state().setTool(Tools.Room);
            ToolStateSkeleton.state().setOptions(4);
            bottomLabel.setText("Tool: Room, Option: Rectangle");
        });

        MenuItem pentagonMenuItem = new MenuItem("Pentagon");
        pentagonMenuItem.setOnAction(event -> {
            ToolStateSkeleton.state().setTool(Tools.Room);
            ToolStateSkeleton.state().setOptions(5);
            bottomLabel.setText("Tool: Room, Option: Pentagon");
        });

        MenuItem hexaconMenuItem = new MenuItem("Hexacon");
        hexaconMenuItem.setOnAction(event -> {
            ToolStateSkeleton.state().setTool(Tools.Room);
            ToolStateSkeleton.state().setOptions(6);
            bottomLabel.setText("Tool: Room, Option: Hexacon");
        });

        roomMenuButton.getItems().addAll(lineMenuItem, triangleMenuItem, rectangleMenuItem, pentagonMenuItem, hexaconMenuItem);
        roomMenuButton.setId("Room");

        Button pathButton = new Button();
        pathButton.setId("Path");

        Button eraseButton = new Button();
        eraseButton.setOnMouseClicked(event -> {
            ToolStateSkeleton.state().setTool(Tools.Erase);
            bottomLabel.setText("Tool: Erase");
        });
        eraseButton.setId("Erase");

        Button doorButton = new Button();
        doorButton.setId("Door");
        doorButton.setOnMouseClicked(event -> primaryStage.hide());

        VBox vBox = new VBox(selectButton, moveButton, roomMenuButton, pathButton, eraseButton, doorButton);

        ToolBar leftToolBar = new ToolBar(vBox);


        map = new MapAreaSkeleton();

        root.setTop(menuBar);
        root.setCenter(map);
        root.setLeft(leftToolBar);
        root.setBottom(bottomToolBar);

        Scene scene = new Scene(root, 800, 800);
        scene.getStylesheets().add(new File("resources/css/style.css").toURI().toString());
        primaryStage.addEventHandler(KeyEvent.KEY_RELEASED, event -> {
            if (event.getCode() == KeyCode.ESCAPE) primaryStage.hide();
        });
        primaryStage.setScene(scene);
        primaryStage.setTitle("Map Maker Skeleton");
        primaryStage.show();
    }

    private void displayCredit() {
        displayAlert("Credit", loadFile(CREDITS_PATH));
    }

    private String loadFile(String path) {
        String message = "";
        try {
            message = Files.lines(Paths.get(path)).reduce("", (a, b) -> a + System.lineSeparator() + b + System.lineSeparator());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return message;
    }

    private void displayAlert(String title, String message) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle(title);
        alert.setContentText(message);
        alert.show();
    }

    @Override
    public void stop() throws Exception {
        super.stop();
    }

    private MenuItem createMenuItem(String name, EventHandler<ActionEvent> handler) {
        Label icon = new Label();
        icon.setId(name + "-icon");
        MenuItem item = new MenuItem(name, icon);
        item.setOnAction(handler);
        item.setId(name);
        return item;
    }

    private void showInputDialog(String title, String content, String match, Consumer<String> action) {
        TextInputDialog input = new TextInputDialog();
        input.setTitle(title);
        input.setHeaderText(null);
        input.setContentText(content);
        input.getEditor().textProperty().addListener((value, oldV, newV) -> {
            if (!newV.isEmpty() && !Pattern.matches(match, newV)) {
                input.getEditor().setText(oldV);
            }
        });
        input.showAndWait().ifPresent(e -> {
            if (e.matches(match)) action.accept(e);
        });
    }

    private String loadFile(String path, String seprator) {
        try {
            return Files.lines(Paths.get(path)).reduce("", (a, b) -> a + seprator + b);
        } catch (IOException e) {
            e.printStackTrace();
            return "\"" + path + "\" was probably not found" + "\nmessage: " + e.getMessage();
        }
    }

    private void saveMap(Stage primary) {
        File file = getFileChooser(primary, true);
        if (file == null) return;
        try {
            if (!file.exists()) file.createNewFile();
            Files.write(file.toPath(), map.convertToString().getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void loadMap(Stage primary) {
        File file = getFileChooser(primary, false);
        if (file == null) return;
        try {
            if (!file.exists()) file.createNewFile();
            //no parallel here but this is safer
            AtomicInteger index = new AtomicInteger(0);
            //index.getAndIncrement()/5 every 5 elements the value division increases by 1
            //allowing for every 5 element placed in the same key
            map.convertFromString(Files.lines(file.toPath()).collect(Collectors.groupingBy(l -> index.getAndIncrement() / 5)));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private File getFileChooser(Stage primary, boolean save) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().add(new ExtensionFilter("Maps", "*.map"));
        fileChooser.setInitialDirectory(Paths.get(MAPS_DIRECTORY).toFile());
        return save ? fileChooser.showSaveDialog(primary) : fileChooser.showOpenDialog(primary);
    }
}