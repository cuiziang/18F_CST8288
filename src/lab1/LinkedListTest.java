package lab1;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LinkedListTest {

    private Queue<Object> ll;
    private Object obj1;

    @BeforeEach
    void setUp() {
        ll = new LinkedList<>();
        obj1 = new Object();
        ll.offer("one");
        ll.offer(1);
        ll.offer(obj1);
    }

    @Test
    @DisplayName("Offer test")
    void testOffer() {
        assertTrue(ll.offer("two"));

        assertSame("one", ll.poll());
        assertSame(1, ll.poll());
        assertSame(obj1, ll.poll());
        assertSame("two", ll.poll());
        assertThrows(NullPointerException.class, () -> ll.offer(null));
    }

    @Test
    @DisplayName("Poll test")
    void testPoll() {
        assertSame("one", ll.poll());

        assertSame(1, ll.poll());

        assertSame(obj1, ll.poll());

        assertThrows(IllegalStateException.class, () -> ll.poll());
    }

    @Test
    @DisplayName("Peek test")
    void testPeek() {
        assertSame("one", ll.peek());

        ll.poll();
        assertSame(1, ll.peek());

        ll.poll();
        assertSame(obj1, ll.peek());

        ll.poll();
        assertThrows(IllegalStateException.class, () -> ll.peek());
    }
}