package midterm;

public class Student {

    private final long ID;
    private String fName;
    private String lname;

    public Student(String fName, String lname, long id) {
        this.ID = id;
        this.fName = fName;
        this.lname = lname;
    }

    public void setName(String fName, String lName) {
        if (fName != null && lname != null) {
            this.fName = fName;
            this.lname = lName;
        }
    }

    public long getId() {
        return ID;
    }

    public String getfName() {
        return fName;
    }

    public String getLname() {
        return lname;
    }

    @Override
    public String toString() {
        return String.format("Student: %s %s %s", getId(), getfName(), getLname());
    }
}
