package midterm;

import javafx.application.Application;
import javafx.collections.ListChangeListener.Change;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.IntStream;

import static java.lang.Thread.sleep;

public class StudentView extends Application {

    private Button start, stop, clear;
    private TextArea area;
    private StudentManager manager;
    private AtomicBoolean alive;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void init() throws Exception {
        super.init();
        this.alive = new AtomicBoolean();
        this.manager = new StudentManager();
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        area = new TextArea();
        area.setEditable(false);

        start = new Button("Start");
        stop = new Button("Stop");
        clear = new Button("Clear");

        GridPane controlPane = new GridPane();
        controlPane.setHgap(5);
        controlPane.setVgap(5);
        controlPane.setPadding(new Insets(5));
        controlPane.add(start, 0, 0);
        controlPane.add(stop, 0, 1);
        controlPane.add(clear, 0, 2);

        BorderPane rootPane = new BorderPane();
        rootPane.setCenter(area);
        rootPane.setRight(controlPane);

        Scene scene = new Scene(rootPane, 500, 300);
        primaryStage.setScene(scene);
        primaryStage.setTitle("Student MVC-O");
        primaryStage.addEventHandler(KeyEvent.KEY_RELEASED, (KeyEvent event) -> {
            if (KeyCode.ESCAPE == event.getCode()) {
                primaryStage.hide();
            }
        });
        primaryStage.show();
        registerListeners();
    }

    @Override
    public void stop() throws Exception {
        super.stop();
        this.alive.set(false);
    }

    public void registerListeners() {

        this.start.setOnAction(this::startDataThread);
        this.stop.setOnAction(this::stopDataThread);
        this.clear.setOnAction(e -> area.clear());

        this.manager.getStudents().addListener(this::createStudents);

    }

    public void stopDataThread(ActionEvent e) {
        this.alive.set(false);

    }

    public void startDataThread(ActionEvent e) {

        new Thread(() -> {
            this.alive.set(true);
            IntStream.iterate(0, i -> i < 100 && alive.get(), i -> i + 1).forEach(i -> {
                try {
                    sleep(50);
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
                this.manager.addStudent(String.valueOf(i), String.valueOf(i));
            });
        }).start();

    }

    public void createStudents(Change<? extends Student> c) {
        if (c.next()) {
            c.getAddedSubList().forEach(str -> area.appendText(str.toString()+"\n"));
        }
    }
}
