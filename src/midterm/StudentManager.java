package midterm;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.LinkedList;
import java.util.Random;

public class StudentManager {

    private Random rand;
    private ObservableList<Student> student;

    public StudentManager() {
        this.rand = new Random();
        this.student = FXCollections.observableList(new LinkedList<>());
    }

    public void addStudent(String fName, String lName) {
        this.student.add(new Student(fName, lName, rand.nextLong()));
    }

    public ObservableList<Student> getStudents() {
        return this.student;
    }
}